// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use super::{canceled_menu, hotkey_button, FloatingMenuPos, FloatingMenuState};
use crate::mouse_interaction::active::MakeItemActive;
use crate::mouse_interaction::Selection;
use crate::node::MoveToCursor;
use crate::vismut::{ActiveDagId, GuiEngine};
use crate::{
    drag_drop::Draggable,
    mouse_interaction::select::{ReplaceSelection, Selected},
    scan_code_input::{ScanCode, ScanCodeInput},
    shared::NodeAddressComponent,
    undo::prelude::UndoCommandManager,
    undo::{node::AddNode, prelude::*},
    DragDropMode, GrabToolType, ToolState,
};
use bevy::prelude::*;
use bevy_egui::{
    egui::{self, Ui},
    EguiContext,
};
use vismut_core::node::bool::NodeBool;
use vismut_core::node::distribute_gray::NodeDistributeGray;

use vismut_core::node::float_to_gray::NodeFloatToGray;
use vismut_core::node::gradient::NodeGradient;
use vismut_core::node::image::NodeImage;
use vismut_core::node::int::NodeInt;
use vismut_core::node::math_gray::NodeMathGray;
use vismut_core::node::merge_rgba::NodeMergeRgba;
use vismut_core::node::output_rgba::NodeOutputRgba;
use vismut_core::node::resize::NodeResize;
use vismut_core::node::sample_uv::NodeSampleUv;
use vismut_core::node::split_rgba::NodeSplitRgba;
use vismut_core::node::Node;
use vismut_core::prelude::*;

/// Brings up a dropdown menu with all the nodes that can be created.
///
/// Returns true if something was clicked.
pub(super) fn add_node_menu(
    undo_command_manager: &mut UndoCommandManager,
    move_to_cursor: &mut MoveToCursor,
    engine: &mut Engine,
    dag_id: DagId,
    sc_input: &mut ScanCodeInput,
    ui: &mut Ui,
    drag_drop_mode: &mut DragDropMode,
) -> bool {
    let mut clicked = false;

    let nodes: Vec<Box<dyn Node>> = if hotkey_button(ui, &mut *sc_input, "Float", ScanCode::KeyF) {
        clicked = true;
        vec![NodeFloat::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Float to Gray", ScanCode::KeyV) {
        clicked = true;
        vec![NodeFloatToGray::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Image", ScanCode::KeyI) {
        clicked = true;
        vec![NodeImage::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Gradient", ScanCode::KeyG) {
        clicked = true;
        vec![NodeGradient::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Bool", ScanCode::KeyB) {
        clicked = true;
        vec![NodeBool::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Integer", ScanCode::KeyN) {
        clicked = true;
        vec![NodeInt::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Merge RGBA", ScanCode::KeyM) {
        clicked = true;
        vec![NodeMergeRgba::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Output RGBA", ScanCode::KeyO) {
        clicked = true;
        vec![NodeOutputRgba::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Split RGBA", ScanCode::KeyS) {
        clicked = true;
        vec![NodeSplitRgba::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Sample UV", ScanCode::KeyU) {
        clicked = true;
        vec![NodeSampleUv::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Math Gray", ScanCode::KeyT) {
        clicked = true;
        vec![NodeMathGray::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Distribute Gray", ScanCode::KeyD) {
        clicked = true;
        vec![NodeDistributeGray::new()]
    } else if hotkey_button(ui, &mut *sc_input, "Resize", ScanCode::KeyR) {
        clicked = true;
        vec![NodeResize::new()]
    } else {
        vec![]
    };

    // let node_types = if hotkey_button(ui, &mut *sc_input, "Image…", ScanCode::KeyI) {
    //     clicked = true;
    //
    //     let file_dialog = FileDialog::new()
    //         .add_filter(
    //             "Image",
    //             &["bmp", "gif", "jpg", "jpeg", "png", "tga", "tiff"],
    //         )
    //         .show_open_multiple_file();
    //
    //     if let Ok(path_bufs) = file_dialog {
    //         path_bufs
    //             .into_iter()
    //             .map(|x| NodeType::Image(ImageNode(x)))
    //             .collect::<Vec<NodeType>>()
    //     } else {
    //         error!("could not open file dialog");
    //         Vec::new()
    //     }
    // } else if hotkey_button(ui, &mut *sc_input, "Output", ScanCode::KeyO) {
    //     clicked = true;
    //     vec![NodeType::OutputRgba(OutputRgbaNode("untitled".into()))]
    // } else if hotkey_button(ui, &mut *sc_input, "Split", ScanCode::KeyS) {
    //     clicked = true;
    //     vec![NodeType::SplitRgba(SplitRgbaNode(Resize::default()))]
    // } else if hotkey_button(ui, &mut *sc_input, "Merge", ScanCode::KeyM) {
    //     clicked = true;
    //     vec![NodeType::MergeRgba(MergeRgbaNode(Resize::default()))]
    // } else if hotkey_button(ui, &mut *sc_input, "Grayscale", ScanCode::KeyG) {
    //     clicked = true;
    //     vec![NodeType::Grayscale(GrayscaleNode(1.0))]
    // } else if hotkey_button(ui, &mut *sc_input, "Grayscale Info", ScanCode::KeyT) {
    //     clicked = true;
    //     vec![NodeType::GrayscaleInfo(GrayscaleInfoNode)]
    // } else if hotkey_button(ui, &mut *sc_input, "UV Coordinates", ScanCode::KeyU) {
    //     clicked = true;
    //     vec![NodeType::Uv(UvNode)]
    // } else if hotkey_button(ui, &mut *sc_input, "Math Grayscale", ScanCode::KeyB) {
    //     clicked = true;
    //     vec![NodeType::MathGrayscale(MathGrayscaleNode(
    //         Resize::default(),
    //         MathOperation::default(),
    //     ))]
    // } else if hotkey_button(ui, &mut *sc_input, "Distribute Grayscale", ScanCode::KeyD) {
    //     clicked = true;
    //     vec![NodeType::DistributeGrayscale(DistributeGrayscaleNode)]
    // } else if hotkey_button(ui, &mut *sc_input, "Int", ScanCode::KeyN) {
    //     clicked = true;
    //     vec![NodeType::Int(IntNode(0))]
    // } else if hotkey_button(ui, &mut *sc_input, "Float", ScanCode::KeyF) {
    //     clicked = true;
    //     vec![NodeType::Float(FloatNode(0.0))]
    // } else if hotkey_button(ui, &mut *sc_input, "Sample UV", ScanCode::KeyV) {
    //     clicked = true;
    //     vec![NodeType::SampleUv(SampleUvNode(Resize::default()))]
    // } else {
    //     Vec::new()
    // };

    if !nodes.is_empty() {
        create_nodes(undo_command_manager, move_to_cursor, engine, dag_id, &nodes);
    }

    if clicked {
        *drag_drop_mode = DragDropMode::Pressed;
    }

    clicked
}

#[allow(clippy::too_many_arguments)]
pub(super) fn floating_add_menu(
    mut undo_command_manager: ResMut<UndoCommandManager>,
    mut engine: ResMut<GuiEngine>,
    mut move_to_cursor: ResMut<MoveToCursor>,
    dag_id: Res<ActiveDagId>,
    i_mouse_button: Res<Input<MouseButton>>,
    mut sc_input: ResMut<ScanCodeInput>,
    mut egui_context: ResMut<EguiContext>,
    mut floating_menu_state: ResMut<State<FloatingMenuState>>,
    floating_menu_pos: Res<FloatingMenuPos>,
    mut ignore_fast_mouse_release: ResMut<DragDropMode>,
) {
    let mut chose_something = false;

    egui::Area::new("add_menu")
        .fixed_pos(floating_menu_pos.0)
        .show(egui_context.ctx_mut(), |ui| {
            chose_something = add_node_menu(
                &mut undo_command_manager,
                &mut move_to_cursor,
                &mut engine,
                **dag_id,
                &mut sc_input,
                ui,
                &mut ignore_fast_mouse_release,
            );
        });

    if canceled_menu(&i_mouse_button, &sc_input) || chose_something {
        floating_menu_state.set(FloatingMenuState::None).unwrap();
    }
}

/// Creates the given nodes in the engine.
pub fn create_nodes(
    undo_command_manager: &mut UndoCommandManager,
    move_to_cursor: &mut MoveToCursor,
    engine: &mut Engine,
    dag_id: DagId,
    nodes: &[Box<dyn Node>],
) {
    if !nodes.is_empty() {
        let mut active_node_address = None;

        for node_type in nodes {
            let node_address = engine.new_address(dag_id).expect("DAG should always exist");
            undo_command_manager.push(Box::new(AddNode::new(
                node_address,
                node_type.clone(),
                Vec2::ZERO,
            )));

            if active_node_address.is_none() {
                active_node_address = Some(node_address);
            }
        }

        grab_new_nodes(undo_command_manager, move_to_cursor);

        if let Some(node_address) = active_node_address {
            undo_command_manager.push(Box::new(MakeItemActive(Selection::Node(node_address))));
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct DragToolUndo;

impl UndoCommand for DragToolUndo {
    fn command_type(&self) -> crate::undo::UndoCommandType {
        crate::undo::UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut tool_state = world.resource_mut::<State<ToolState>>();
        let _ = tool_state.overwrite_replace(ToolState::Grab(GrabToolType::Node));
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is not saved on the undo stack");
    }
}

#[derive(Copy, Clone, Debug)]
struct SelectNew;

impl UndoCommand for SelectNew {
    fn command_type(&self) -> crate::undo::UndoCommandType {
        crate::undo::UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut query =
            world.query_filtered::<&NodeAddressComponent, (With<Draggable>, Added<NodeAddressComponent>)>();
        let new_node_addresses = query
            .iter(world)
            .map(|node_address| Selection::Node(node_address.0))
            .collect();

        undo_command_manager.push_front(Box::new(ReplaceSelection(new_node_addresses)));
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is not saved on the undo stack");
    }
}

/// The sneaky variant is not saved on the undo stack. Can probably be replaced with a command that
/// removes the most recent command from the undo stack.
#[derive(Copy, Clone, Debug)]
struct DeselectSneaky;

impl UndoCommand for DeselectSneaky {
    fn command_type(&self) -> crate::undo::UndoCommandType {
        crate::undo::UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut query = world.query_filtered::<Entity, With<Selected>>();

        for entity in query.iter(world).collect::<Vec<Entity>>() {
            world.entity_mut(entity).remove::<Selected>();
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is not saved on the undo stack");
    }
}

/// Grabs any nodes that were just created.
pub fn grab_new_nodes(
    undo_command_manager: &mut UndoCommandManager,
    move_to_cursor: &mut MoveToCursor,
) {
    move_to_cursor.0 = true;
    undo_command_manager.push(Box::new(SelectNew));
    undo_command_manager.push(Box::new(DragToolUndo));
}
