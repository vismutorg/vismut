// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::undo::undo_command_manager::Dirtied;
use crate::{shared::NodeAddressComponent, AmbiguitySet, CustomStage, Thumbnail};
use bevy::window::RequestRedraw;
use bevy::{
    prelude::*,
    render::render_resource::{Extent3d, TextureDimension, TextureFormat},
};
use std::sync::mpsc::TryRecvError;
use std::sync::{mpsc, Mutex};

use crate::vismut::{ActiveDagId, GuiEngine};
use vismut_core::prelude::*;

#[derive(Component, Deref, DerefMut)]
pub struct ThumbnailReceiver(Mutex<mpsc::Receiver<SlotData>>);

pub(crate) struct ThumbnailPlugin;

impl Plugin for ThumbnailPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .label(CustomStage::Apply)
                .after(CustomStage::Update)
                .with_system(add_thumbnail_receivers.ambiguous_with(AmbiguitySet))
                .with_system(
                    receive_thumbnails
                        .ambiguous_with(AmbiguitySet)
                        .after(add_thumbnail_receivers),
                )
                .with_system(redraw_while_processing),
        );
    }
}

/// Adds thumbnail receivers to nodes that are being processed.
fn add_thumbnail_receivers(
    mut commands: Commands,
    mut engine: ResMut<GuiEngine>,
    q_node: Query<(Entity, &NodeAddressComponent), Added<Dirtied>>,
    q_thumbnail: Query<(Entity, &Parent), With<Thumbnail>>,
) {
    for (node_entity, node_address) in &q_node {
        let thumbnail_entity = q_thumbnail
            .iter()
            .find(|(_, parent)| parent.get() == node_entity)
            .map(|(entity, _)| entity);
        let receiver = engine.buffer_rgba_receiver(node_address.with_slot_id(SlotId(0)));

        if let (Some(thumbnail_entity), Ok(receiver)) = (thumbnail_entity, receiver) {
            commands
                .entity(thumbnail_entity)
                .insert(ThumbnailReceiver(Mutex::new(receiver)));
        }
    }
}

/// Receives and applies thumbnails where they belong.
fn receive_thumbnails(
    mut commands: Commands,
    mut images: ResMut<Assets<Image>>,
    mut q_thumbnail_receiver: Query<(Entity, &mut Handle<Image>, &ThumbnailReceiver)>,
) {
    for (entity, mut handle_image, thumbnail_receiver) in &mut q_thumbnail_receiver {
        if let Ok(thumbnail_receiver) = thumbnail_receiver.lock() {
            let try_recv = thumbnail_receiver.try_recv();

            if let Ok(slot_data) = try_recv {
                let (size, buffer) = if let SlotData::SlotImage(slot_image) = slot_data {
                    (slot_image.size(), slot_image.to_u8())
                } else {
                    warn!("slot type does not have a thumbnail");
                    commands.entity(entity).remove::<ThumbnailReceiver>();
                    continue;
                };

                let (width, height) = size.result();

                let image = Image::new(
                    Extent3d {
                        width,
                        height,
                        depth_or_array_layers: 1,
                    },
                    TextureDimension::D2,
                    buffer,
                    TextureFormat::Rgba8UnormSrgb,
                );

                let new_handle_image = images.add(image);
                *handle_image = new_handle_image;

                info!("applied thumbnail");
                commands.entity(entity).remove::<ThumbnailReceiver>();
            } else if let Err(TryRecvError::Disconnected) = try_recv {
                warn!("image handle disconnected, removing it");
                commands.entity(entity).remove::<ThumbnailReceiver>();
            }
        } else {
            warn!("could not get mutex lock on ThumbnailReceiver, removing it");
            commands.entity(entity).remove::<ThumbnailReceiver>();
        }
    }
}

/// Sends redraw requests as long as something is being processed.
/// This ensures that the latest information is always shown.
fn redraw_while_processing(
    mut request_redraw: EventWriter<RequestRedraw>,
    engine: Res<GuiEngine>,
    dag_id: Res<ActiveDagId>,
) {
    let all_clean = engine
        .node_states(**dag_id)
        .iter()
        .all(|(_, node_state)| *node_state == NodeState::Clean);
    if !all_clean {
        request_redraw.send(RequestRedraw);
    }
}
