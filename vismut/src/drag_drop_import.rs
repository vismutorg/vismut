// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;
use vismut_core::prelude::NodeType;
use vismut_core::ImageNode;

use crate::drag_drop::node::MoveToCursor;
use crate::menu_bar::add_node::create_nodes;
use crate::vismut::{ActiveDagId, GuiEngine};
use crate::{undo::prelude::UndoCommandManager, AmbiguitySet, CustomStage, ToolState};

pub(crate) struct DragDropImport;

impl Plugin for DragDropImport {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new().after(CustomStage::Setup).with_system(
                drag_drop_import
                    .with_run_criteria(State::on_update(ToolState::None))
                    .ambiguous_with(AmbiguitySet),
            ),
        );
    }
}

fn drag_drop_import(
    mut undo_command_manager: ResMut<UndoCommandManager>,
    mut events: EventReader<FileDragAndDrop>,
    mut engine: ResMut<GuiEngine>,
    dag_id: Res<ActiveDagId>,
    mut move_to_cursor: ResMut<MoveToCursor>,
) {
    let mut new_nodes = Vec::new();

    for event in events.iter() {
        let FileDragAndDrop::DroppedFile { id: _, path_buf } = event else { continue; };

        let new_node = NodeType::Image(ImageNode(path_buf.clone()));

        new_nodes.push(new_node);
    }

    create_nodes(
        &mut undo_command_manager,
        &mut move_to_cursor,
        &mut engine,
        **dag_id,
        &new_nodes,
    );
}
