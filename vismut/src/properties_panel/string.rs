// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::core_translation::NodePropertyTranslatorString;
use crate::properties_panel::SavedProperties;
use crate::undo::prelude::*;

use bevy_egui::egui::Ui;
use vismut_core::node::PropertyData;
use vismut_core::prelude::*;

pub fn display(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    value: &str,
    property_name: &str,
    saved_properties: &mut SavedProperties,
) {
    let value_current = value;
    let PropertyData::String(value_new) = saved_properties
        .properties
        .get_mut(property_name)
        .expect("could not find a property with that name")
        else { panic!("expected a `PropertyData::String`") };

    if ui.text_edit_singleline(value_new).lost_focus() && value_new != value_current {
        undo_command_manager.push(Box::new(GuiUndoCommand::new(
            node_address.to_property_address(property_name),
            NodePropertyTranslatorString(value_current.to_string()),
            NodePropertyTranslatorString(value_new.clone()),
        )));
        undo_command_manager.push(Box::new(Checkpoint));
    }

    ui.end_row();
}
