// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::undo::gui::GuiUndoCommand;
use crate::undo::prelude::{Checkpoint, UndoCommandManager};

use bevy_egui::egui::Ui;

use vismut_core::address::NodeAddress;

use vismut_core::prelude::{MathOperation, NodeType};

use vismut_core::MathGrayscaleNode;

pub fn display(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    node_type: &NodeType,
) {
    let math_operation_current =
        if let NodeType::MathGrayscale(MathGrayscaleNode(_, math_operation)) = node_type {
            *math_operation
        } else {
            return;
        };

    let mut math_operation_new = math_operation_current;

    ui.label("Operation:");
    ui.menu_button(format!("{}", math_operation_current), |ui| {
        if ui.button("Add").clicked() {
            math_operation_new = MathOperation::Add;
            ui.close_menu();
        };
        if ui.button("Subtract").clicked() {
            math_operation_new = MathOperation::Subtract;
            ui.close_menu();
        };
        if ui.button("Multiply").clicked() {
            math_operation_new = MathOperation::Multiply;
            ui.close_menu();
        };
        if ui.button("Divide").clicked() {
            math_operation_new = MathOperation::Divide;
            ui.close_menu();
        };
        if ui.button("Power").clicked() {
            math_operation_new = MathOperation::Power;
            ui.close_menu();
        };
        if ui.button("Square Root").clicked() {
            math_operation_new = MathOperation::SquareRoot;
            ui.close_menu();
        };
        if ui.button("Sin").clicked() {
            math_operation_new = MathOperation::Sin;
            ui.close_menu();
        };
        if ui.button("Cos").clicked() {
            math_operation_new = MathOperation::Cos;
            ui.close_menu();
        };
    });

    if math_operation_current != math_operation_new {
        undo_command_manager.push(Box::new(GuiUndoCommand::new(
            node_address,
            math_operation_current,
            math_operation_new,
        )));
        undo_command_manager.push(Box::new(Checkpoint));
    }

    ui.end_row();
}
