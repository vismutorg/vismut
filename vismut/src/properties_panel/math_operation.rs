// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::core_translation::NodePropertyTranslatorMathOperation;
use crate::undo::prelude::*;
use bevy_egui::egui;
use bevy_egui::egui::Ui;

use vismut_core::prelude::*;

pub fn display(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    value: MathOperation,
    property_name: &str,
) {
    let current_value = value;
    let mut new_value = value;

    egui::containers::ComboBox::from_id_source("math_operation")
        .selected_text(format!("{:?}", new_value))
        .show_ui(ui, |ui| {
            ui.selectable_value(&mut new_value, MathOperation::Add, "Add");
            ui.selectable_value(&mut new_value, MathOperation::Subtract, "Subtract");
            ui.selectable_value(&mut new_value, MathOperation::Multiply, "Multiply");
            ui.selectable_value(&mut new_value, MathOperation::Divide, "Divide");
            ui.selectable_value(&mut new_value, MathOperation::Power, "Power");
            ui.selectable_value(&mut new_value, MathOperation::SquareRoot, "SquareRoot");
            ui.selectable_value(&mut new_value, MathOperation::Sin, "Sin");
            ui.selectable_value(&mut new_value, MathOperation::Cos, "Cos");
            ui.selectable_value(&mut new_value, MathOperation::Max, "Max");
            ui.selectable_value(&mut new_value, MathOperation::Min, "Min");
        });

    if current_value != new_value {
        undo_command_manager.push(Box::new(GuiUndoCommand::new(
            node_address.to_property_address(property_name),
            NodePropertyTranslatorMathOperation(current_value),
            NodePropertyTranslatorMathOperation(new_value),
        )));
        undo_command_manager.push(Box::new(Checkpoint));
    }

    ui.end_row();
}
