// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::core_translation::NodePropertyTranslatorFloat;
use crate::undo::prelude::*;
use bevy_egui::egui;
use bevy_egui::egui::Ui;
use vismut_core::node::VismutFloat;
use vismut_core::prelude::*;

pub fn display(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    value: VismutFloat,
    property_name: &str,
) {
    let value_current = value;
    let mut value_new = value_current;
    let response = ui.add(egui::Slider::new(&mut value_new, 0.0..=512.0));

    if (response.drag_released() || response.lost_focus()) && value_new != value_current {
        undo_command_manager.push(Box::new(GuiUndoCommand::new(
            node_address.to_property_address(property_name),
            NodePropertyTranslatorFloat(value_current),
            NodePropertyTranslatorFloat(value_new),
        )));
        undo_command_manager.push(Box::new(Checkpoint));
    }

    ui.end_row();
}
