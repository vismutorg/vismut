use crate::shared::{BACKPLATE_PADDING, GRID_HEIGHT, GRID_WIDTH};
use crate::GridSize;
use bevy::prelude::*;

/// Updates the size of any "backplates" that has changed. The backplate is the sprite that forms
/// the "foundation" of for instance a node.
pub(super) fn update_backplate(
    mut q_backplate: Query<(&GridSize, &mut Sprite), Changed<GridSize>>,
) {
    for (grid_size, mut sprite) in &mut q_backplate {
        let padding = grid_size.padding;

        let grid_width: u8 = grid_size.width.into();
        let pixel_width: usize =
            GRID_WIDTH * grid_width as usize - BACKPLATE_PADDING * 2 + padding.0;

        let grid_height: u8 = grid_size.height.into();
        let pixel_height: usize =
            GRID_HEIGHT * grid_height as usize - BACKPLATE_PADDING * 2 + padding.1;

        let new_size = Vec2::new(pixel_width as f32, pixel_height as f32);

        sprite.custom_size = Some(new_size);
    }
}
