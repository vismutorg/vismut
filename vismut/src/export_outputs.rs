// This file is part of Vismut.
// Copyright (C) 2022-present  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use std::path::PathBuf;

use bevy::prelude::*;
use native_dialog::FileDialog;
use vismut_core::node::PropertyData;
use vismut_core::pow_two::SizePow2;
use vismut_core::prelude::*;

use crate::vismut::{ActiveDagId, GuiEngine};
use crate::{
    scan_code_input::ScanCodeInput,
    status_bar::{StatusText, Urgency},
    AmbiguitySet, ToolState,
};

#[derive(Resource)]
struct ExportPath(Option<PathBuf>);

#[derive(Resource)]
struct WaitedFrame(usize);

pub(crate) struct ExportOutputsToolPlugin;

impl Plugin for ExportOutputsToolPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(ExportPath(None))
            .insert_resource(WaitedFrame(0))
            .add_system_set_to_stage(
                CoreStage::Update,
                SystemSet::new()
                    .with_system(
                        export_as
                            .with_run_criteria(State::on_update(ToolState::ExportOutputs(true)))
                            .ambiguous_with(AmbiguitySet),
                    )
                    .with_system(
                        export
                            .with_run_criteria(State::on_update(ToolState::ExportOutputs(false)))
                            .ambiguous_with(AmbiguitySet),
                    ),
            );
    }
}

fn export_dialog(scan_code_input: &mut ScanCodeInput) -> Option<PathBuf> {
    scan_code_input.reset_all();

    match FileDialog::new().show_open_single_dir() {
        Ok(path) => path,
        Err(e) => {
            warn!("Unable to get export directory: {:?}\n", e);
            None
        }
    }
}

fn do_export(
    directory: Option<PathBuf>,
    engine: &Engine,
    dag_id: DagId,
    status_text: &mut StatusText,
) {
    if let Some(path) = directory {
        let output_addresses = engine.output_addresses(dag_id).unwrap();
        let mut exported_count: usize = 0;

        for slot_address in output_addresses {
            let mut path = path.clone();

            let size: SizePow2 = match engine.slot_size(slot_address) {
                Ok(s) => s,
                Err(_) => {
                    let message = "unable to get size of slot";
                    warn!(message);
                    status_text.set(Urgency::Warn, message.to_string());
                    return;
                }
            };

            let node = engine
                .node(slot_address.without_slot_id())
                .expect("expected the node to exist");
            let PropertyData::String(file_name) = node.property("name")
                .expect("expected the property to exist")
                else { panic!("expected the name to be a PropertyData::String") };

            path.push(file_name);
            path.set_extension("png");

            let texels = match engine.buffer_rgba(slot_address) {
                Ok(buf) => buf,
                Err(e) => {
                    error!("error when trying to get pixels from image: {:?}", e);
                    continue;
                }
            };

            let (width, height) = size.result();
            let buffer = match image::RgbaImage::from_vec(width, height, texels) {
                None => {
                    error!("output image buffer not big enough to contain texels.");
                    continue;
                }
                Some(buf) => buf,
            };

            match image::save_buffer(&path, &buffer, width, height, image::ColorType::Rgba8) {
                Ok(_) => info!("Image exported to {:?}", path),
                Err(e) => {
                    error!("{}", e);
                    continue;
                }
            }

            exported_count += 1;
        }

        if exported_count == 0 {
            status_text.set(Urgency::Warn, "Nothing to export".to_string());
        } else {
            status_text.set(
                Urgency::None,
                format!("Exported {} outputs to {}", exported_count, path.display()),
            );
        }
    } else {
        info!("cancelled file dialog");
    }
}

fn export_as(
    engine: Res<GuiEngine>,
    dag_id: Res<ActiveDagId>,
    mut tool_state: ResMut<State<ToolState>>,
    mut sc_input: ResMut<ScanCodeInput>,
    mut export_path: ResMut<ExportPath>,
    mut waited_frame: ResMut<WaitedFrame>,
    mut status_text: ResMut<StatusText>,
) {
    if waited_frame.0 > 1 {
        let directory = export_dialog(&mut sc_input);
        export_path.0 = directory.clone();

        do_export(directory, &engine, **dag_id, &mut status_text);

        tool_state.overwrite_replace(ToolState::None).unwrap();
        waited_frame.0 = 0;
    } else {
        waited_frame.0 += 1;
    }
}

fn export(
    engine: Res<GuiEngine>,
    dag_id: Res<ActiveDagId>,
    mut tool_state: ResMut<State<ToolState>>,
    export_path: ResMut<ExportPath>,
    mut waited_frame: ResMut<WaitedFrame>,
    mut status_text: ResMut<StatusText>,
) {
    if waited_frame.0 > 1 {
        if export_path.0.is_none() {
            tool_state
                .overwrite_replace(ToolState::ExportOutputs(true))
                .unwrap();
        } else {
            do_export(export_path.0.clone(), &engine, **dag_id, &mut status_text);
            tool_state.overwrite_replace(ToolState::None).unwrap();
        }
        waited_frame.0 = 0;
    } else {
        waited_frame.0 += 1;
    }
}
