# Vismut

**Vismut will be a procedural texturing tool for Windows and Linux**,
allowing for an entirely non-destructive texture creation workflow.
The non-destructive workflow means you can go back and change your previous decisions,
all the way down to which resolution you want for the texture.

The backend library that Vismut uses is called **Vismut Core**.
It is separate from Vismut,
meaning you can integrate it into your game.
So instead of shipping textures with the game,
you can ship files that generate textures,
and generate them on the client instead.
This can reduce the game's size by a lot!
It can also be used in more creative ways,
like for character customization,
randomly generated textures,
and more.

### [Download](https://orsvarn.com/fileshare/vismut/) | [Community][zulip] | [Manual](https://gitlab.com/vismut-org/vismut/-/blob/main/docs/MANUAL.md)

![Screenshot](screenshot.png)

## Design Goals

These are the three design goals that guide Vismut's development.

- ⏱️ **Speed** -
  Fast to start,
  fluid to use,
  hotkeys for everything
- 😌 **Simplicity** -
  Focused feature set,
  easy to get started,
  user-friendly
- ⚡ **Power**
  (not yet realized) -
  Create any material,
  made for pipeline integration

## Features

The only use case supported in the current version is **manual channel packing**.

### Nodes

- **Image**: Loads an image from disk
- **Output**: Exports an image to disk
- **Split**: Splits an RGBA image into 4 grayscale images
- **Merge**: Merges 4 grayscale images into an RGBA image
- **Grayscale**: Generates a pixel with the given value

## Missing Features

Here are some big features that are currently missing.

- 🎨 Mix node
- ❇️ Noise node
- 📥 Graph node
- 🖼️ 2D preview
- 📷 3D preview
- ⚙️ Command-line tool

## Community & Contributing

The Vismut community lives on the [Vismut Zulip][zulip].
There you can talk to other Vismut users and contributors about anything related to the program.
Welcome! 😀

Vismut uses a simple development process which makes contributing to the project fast and easy.
Check out [Contributing to Vismut](CONTRIBUTING.md) to learn more,
or just submit a merge request!

Everyone needs to follow the [Vismut Code of Conduct](CODE_OF_CONDUCT.md).

## Licenses

This project uses a different license for the frontend application **Vismut** and the backend library **Vismut Core**.

The backend library **Vismut Core**,
which is what you'd integrate into your own project,
is licensed under `MIT OR Apache-2.0`.
This is the standard license in the Rust ecosystem.
The licenses can be found in [LICENSE-APACHE](docs/LICENSE-APACHE) and [LICENSE-MIT](docs/LICENSE-MIT).

The frontend **Vismut**,
which is simply a GUI for Vismut Core,
is licensed under `GPL-3.0-or-later`.
The full license can be found in [vismut/LICENSE](vismut/LICENSE).

[zulip]: https://vismut.zulipchat.com
