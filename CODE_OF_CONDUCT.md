# Vismut Code of Conduct

In the Vismut community,
we don't just try to "not break the rules".
We go the extra step to look out for each other,
and ensure everyone feels welcome.

## Encouraged Behavior

Here are some things our community encourages.
You won't get banned for not following the spirit of this list,
but if you do follow it,
you help grow the community and make it a better place.

- Ensure everyone feels welcome
- Show appreciation for people's contributions
- Keep a friendly and kind tone
- Tell a moderator if you encounter unacceptable behavior

## Unacceptable Behavior

The list below contains things that result in warnings and bans.
The items in the list are unacceptable both in public and private.
The word **content** in this list refers to text,
links,
images,
audio,
video,
speech,
gestures,
and so on.
Here is the list of unacceptable behavior:

- **Discriminating content**,
  this could be based on for instance level of experience,
  gender identity and expression,
  sexual orientation,
  disability,
  appearance,
  body size,
  race,
  ethnicity,
  age,
  religion,
  nationality,
  or other similar characteristic
- **Sexual content**
- **Violent content**
- **Insulting content**
- **Disruptive content**
  (derailing conversations,
  trolling,
  spam,
  encouraging unacceptable behavior,
  and so on)
- **Inappropriate content**
  according to an administrator
- **Personally identifying content**
  provided without their permission
  ("doxing")
- **Recording someone**
  (images,
  audio,
  or similar)
  without their permission
- **Touching someone**
  without their permission

**If you encounter any unacceptable behavior,
or someone makes you feel uncomfortable,
in public or in private,
please contact a moderator.**

When someone behaves in an unacceptable way,
they either receive a warning,
or they get banned.
The length of a ban depends on how serious the behavior was,
as well as on how many warnings and bans the person has received in the past,
and how recent those are.

Being banned from the Vismut community means that you are not allowed to use any official communication channels.
This means you're both banned from contributing to the project,
and from participating in the community,
online and offline.
