# Release Checklist

This document contains a Markdown checklist to help when making releases.
Copying this into an issue on GitLab helps ensure no step is forgotten when releasing a new version of Vismut.

It will change with the needs of the project.

```txt
## Prepare release
- Release commit
    - [ ] Update MANUAL.md
    - [ ] Update CHANGELOG.md
    - [ ] Update version in Cargo.toml
    - [ ] Take new screenshot
    - [ ] Update README.md
    - [ ] Create commit
- [ ] `git tag vx.y.z`
- [ ] `git push upstream --tags`

## Release
- [ ] Wait until CI is finished on GitLab,
      a release is created automatically
- [ ] Paste changelog in release notes
- [ ] Announce
```
