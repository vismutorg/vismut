use crate::address::DagId;
use crate::dag::Dag;

#[derive(Clone)]
pub(crate) struct BlueprintDag {
    pub dag_id: DagId,
    pub dag: Dag,
    pub one_shot: bool,
}

impl BlueprintDag {
    pub const fn new(dag_id: DagId, dag: Dag, one_shot: bool) -> Self {
        Self {
            dag_id,
            dag,
            one_shot,
        }
    }
}
