use crate::error::{Result, VismutError};
use crate::node::PropertyData;
use crate::prelude::*;
use std::fmt::{Display, Formatter};

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum SlotType {
    Bool,
    Float,
    Gray,
    Int,
    Rgba,
    String,
}

impl Default for SlotType {
    fn default() -> Self {
        Self::Gray
    }
}

impl Display for SlotType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let text = if let Self::Gray = self {
            "gray"
        } else {
            "rgba"
        };

        write!(f, "{}", text)
    }
}

impl From<&PropertyData> for SlotType {
    fn from(value: &PropertyData) -> Self {
        match value {
            PropertyData::Bool(_) => Self::Bool,
            PropertyData::Float(_) => Self::Float,
            PropertyData::Gray(_) => Self::Gray,
            PropertyData::Int(_) => Self::Int,
            PropertyData::MathOperation(_) => {
                panic!("can't create a `SlotData` from a `PropertyData::MathOperation`")
            }
            PropertyData::Rgba(_) => Self::Rgba,
            PropertyData::ResizeFilter(_) => {
                panic!("can't create a `SlotData` from a `PropertyData::ResizeFilter`")
            }
            PropertyData::ResizePolicy(_) => {
                panic!("can't create a `SlotData` from a `PropertyData::ResizePolicy`")
            }
            PropertyData::String(_) => Self::String,
        }
    }
}

impl SlotType {
    pub fn compatible(&self, other: Self) -> Result<()> {
        if *self == other {
            Ok(())
        } else {
            Err(VismutError::IncompatibleSlotTypes(*self, other))
        }
    }
}

#[derive(Clone, Debug)]
pub struct Slot {
    pub slot_type: SlotType,
    pub slot_id: SlotId,
    pub name: String,
}

impl Slot {
    pub fn new(slot_type: SlotType, slot_id: SlotId, name: &str) -> Self {
        Self {
            slot_type,
            slot_id,
            name: name.to_string(),
        }
    }
}
