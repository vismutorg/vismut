use crate::live::address::{LiveNodeId, LiveSlotAddress};
use std::fmt::Debug;
use thiserror::Error;

pub type LiveResult<T> = std::result::Result<T, LiveError>;

#[derive(Error, Debug)]
pub enum LiveError {
    #[error("could not find a `LiveNode` with id `{0}`")]
    InvalidLiveNodeId(LiveNodeId),
    #[error("could not find a sideless slot with address `{0}`")]
    InvalidLiveSlotAddressSideless(LiveSlotAddress),
}
