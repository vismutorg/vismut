use crate::address::{NodeAddress, SlotId};

use crate::error::{Result, VismutError};
use crate::live::address::LiveNodeId;
use crate::live::slot_data::SlotData;

use std::sync::mpsc;

use crate::dag::DagProperties;
use crate::node::Node;

/// A package that is ready for processing.
///
/// While this type is exposed in the public interface for this library,
/// it is not meant to be used from the outside.
#[doc(hidden)]
#[derive(Debug)]
pub struct ProcessData {
    pub(crate) sender: mpsc::Sender<FinishedProcessing>,
    pub(crate) live_node_id: LiveNodeId,
    pub(crate) slot_data_package: Vec<(SlotId, SlotData)>,
    pub(crate) dag_properties: DagProperties,
}

impl ProcessData {
    pub(crate) fn send(self, slot_data_packages: Vec<(SlotId, SlotData)>) -> Result<()> {
        self.sender
            .send(FinishedProcessing {
                live_node_id: self.live_node_id,
                slot_data_packages,
            })
            .map_err(|_| VismutError::SendError)
    }

    /// Takes a list of `SlotId`s and returns a list of their corresponding `SlotData`s. It returns
    /// `None` for each `SlotContent` that does not exist.
    pub(crate) fn get_slot_datas(&self, slot_ids: &[SlotId]) -> Vec<Option<&SlotData>> {
        let mut output = Vec::with_capacity(slot_ids.len());

        for slot_id in slot_ids {
            let slot_image = self
                .slot_data_package
                .iter()
                .find(|(slot_id_cmp, _)| slot_id_cmp == slot_id)
                .map(|(_, slot_image)| slot_image);
            output.push(slot_image);
        }

        output
    }
}

#[derive(Debug, PartialEq)]
pub(crate) struct FinishedProcessing {
    pub live_node_id: LiveNodeId,
    pub slot_data_packages: Vec<(SlotId, SlotData)>,
}

#[derive(Debug)]
pub(crate) struct LiveNode {
    pub creator_address: NodeAddress,
    pub live_node_id: LiveNodeId,
    pub node: Box<dyn Node>,
}

impl PartialEq for LiveNode {
    fn eq(&self, other: &Self) -> bool {
        self.creator_address == other.creator_address
            && self.live_node_id == other.live_node_id
            && *self.node == *other.node
    }
}
