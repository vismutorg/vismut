use serde::{Deserialize, Serialize};

use crate::pow_two::{Pow2Relative, SizePow2};
use std::fmt::{Display, Formatter};

/// Attached to nodes that deal with buffers to allow them to be resized.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Resize {
    pub resize_policy: ResizePolicy,
    pub resize_filter: ResizeFilter,
}

impl Default for Resize {
    fn default() -> Self {
        Self::new(ResizePolicy::default(), ResizeFilter::default())
    }
}

impl Display for Resize {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "(ResizePolicy: {}, ResizeFilter: {})",
            self.resize_policy, self.resize_filter
        )
    }
}

impl Resize {
    pub const fn new(resize_policy: ResizePolicy, resize_filter: ResizeFilter) -> Self {
        Self {
            resize_policy,
            resize_filter,
        }
    }
}

/// Determines how the size of an output is determined.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum ResizePolicy {
    /// Size is decided in relation to the first connected input.
    RelativeToInput(Pow2Relative),
    /// Size is decided in relation to the parent.
    RelativeToGraph(Pow2Relative),
    /// Size is set to a specific value, regardless of its environment.
    Absolute(SizePow2),
}

impl Default for ResizePolicy {
    fn default() -> Self {
        Self::RelativeToInput(Pow2Relative::new(0))
    }
}

impl Display for ResizePolicy {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let text = match self {
            Self::RelativeToInput(pow_2_relative) => format!("RelativeToInput({})", pow_2_relative),
            Self::RelativeToGraph(pow_2_relative) => {
                format!("RelativeToParent({})", pow_2_relative)
            }
            Self::Absolute(pow_2) => format!("Absolute({})", pow_2),
        };

        write!(f, "{}", text)
    }
}

impl ResizePolicy {
    /// Returns the size that would result from this `ResizePolicy`.
    ///
    /// It might depend on the size of the input and the size of the graph the node is in,
    /// so those variables need to be supplied.
    pub(crate) fn size(&self, input_size: SizePow2, graph_size: SizePow2) -> SizePow2 {
        match self {
            Self::RelativeToInput(size) => input_size.saturating_add(*size),
            Self::RelativeToGraph(size) => graph_size.saturating_add(*size),
            Self::Absolute(size) => *size,
        }
    }

    /// Does the same thing as `size()`,
    /// but treats the `RelativeToInput` case as if it's `RelativeToGraph`.
    pub(crate) fn size_no_input(&self, graph_size: SizePow2) -> SizePow2 {
        self.size(graph_size, graph_size)
    }
}

/// Determines how something is filtered when it gets resized.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum ResizeFilter {
    /// Nearest Neighbor interpolation, or in other words: no interpolation.
    Nearest,
    /// Bilinear interpolation, linearly interpolates when upscaling, uses "box filtering" when
    /// downscaling, which averages pixels.
    Bilinear,
}

impl Default for ResizeFilter {
    fn default() -> Self {
        Self::Bilinear
    }
}

impl Display for ResizeFilter {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let text = match self {
            Self::Nearest => "Nearest",
            Self::Bilinear => "Bilinear",
        };

        write!(f, "{}", text)
    }
}
