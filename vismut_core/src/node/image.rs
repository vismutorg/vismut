use super::prelude::*;
use crate::error::{Result, VismutError};
use crate::live::dag::LiveDag;
use crate::pow_two::closest_pow_2;
use crate::prelude::*;
use image::imageops::FilterType;
use image::{DynamicImage, ImageBuffer};
use std::path::Path;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeImage {
    properties: NodeProperties,
}

const PATH: &str = "path";
const OUTPUT: &str = "output";

#[typetag::serde]
impl Node for NodeImage {
    fn new_unboxed() -> Self {
        NodeImage {
            properties: NodeProperties::new(vec![NodeProperty::new(
                PATH,
                PropertyData::String(String::new()),
            )]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![Slot::new(SlotType::Rgba, SlotId(0), OUTPUT)]
    }

    fn title(&self) -> String {
        "Image".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeImage {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        let live_node_id = live_dag.insert(creator_address, Box::new(self.clone()));

        live_dag.map_corresponding_name(creator_address, live_node_id, Side::Output, self, OUTPUT);
    }

    fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        let PropertyData::String(path) = self.property(PATH)
            .unwrap_or_else(|_| panic!("expected a property named {}", PATH))
            else { panic!("expected this to be a PropertyData::String" )};
        let path = path.clone();

        let thread_pool = thread_pool.lock().unwrap();
        thread_pool.execute(move || {
            let slot_image = match read_slot_image(path) {
                Ok(slot_image) => slot_image,
                Err(_) => SlotImage::Rgba([
                    Arc::new(pixel_buffer(1.0)),
                    Arc::new(pixel_buffer(0.0)),
                    Arc::new(pixel_buffer(1.0)),
                    Arc::new(pixel_buffer(1.0)),
                ]),
            };

            let slot_data = SlotData::SlotImage(slot_image);

            process_data.send(vec![(SlotId(0), slot_data)]).unwrap();
        });
    }
}

fn read_slot_image<P: AsRef<Path>>(path: P) -> Result<SlotImage> {
    fn pop_buffer_to_arc(
        width: u32,
        height: u32,
        buffers: &mut Vec<Buffer>,
        default: f32,
    ) -> Arc<Buffer> {
        Arc::new(
            buffers
                .pop()
                .or_else(|| {
                    ImageBuffer::from_raw(width, height, vec![default; (width * height) as usize])
                })
                .unwrap(),
        )
    }

    let image = ::image::open(path)?;
    let width = closest_pow_2(image.width());
    let height = closest_pow_2(image.height());
    let image = image.resize_exact(width, height, FilterType::Triangle);

    let mut buffers = deconstruct_image(&image);

    match buffers.len() {
        1 => Ok(SlotImage::Gray(Arc::new(buffers.pop().unwrap()))),
        4 => {
            let (a, b, g, r) = (
                pop_buffer_to_arc(width, height, &mut buffers, 0.0),
                pop_buffer_to_arc(width, height, &mut buffers, 0.0),
                pop_buffer_to_arc(width, height, &mut buffers, 0.0),
                pop_buffer_to_arc(width, height, &mut buffers, 1.0),
            );
            Ok(SlotImage::Rgba([r, g, b, a]))
        }
        _ => Err(VismutError::InvalidBufferCount {
            actual: 0,
            expected: "1 or 4".to_string(),
        }),
    }
}

fn deconstruct_image(image: &DynamicImage) -> Vec<Buffer> {
    let pixels = image.as_flat_samples_u8().unwrap().samples;
    let (width, height) = (image.width(), image.height());
    let pixel_count = (width * height) as usize;
    let channel_count = pixels.len() / pixel_count;
    let max_channel_count = 4;
    let mut pixel_vectors: Vec<Vec<f32>> = Vec::with_capacity(max_channel_count);

    for _ in 0..max_channel_count {
        pixel_vectors.push(Vec::with_capacity(pixel_count));
    }

    let mut current_channel = 0;

    for component in pixels {
        pixel_vectors[current_channel].push(VismutPixel::from(*component) / 255.);
        current_channel = (current_channel + 1) % channel_count;
    }

    for (i, item) in pixel_vectors
        .iter_mut()
        .enumerate()
        .take(max_channel_count)
        .skip(channel_count)
    {
        *item = match i {
            3 => vec![1.; pixel_count],
            _ => vec![0.; pixel_count],
        }
    }

    pixel_vectors
        .into_iter()
        .map(|p_vec| {
            ImageBuffer::from_raw(width, height, p_vec)
                .expect("A bug in the deconstruct_image function caused a crash")
        })
        .collect()
}
