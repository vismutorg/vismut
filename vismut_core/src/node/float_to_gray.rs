use crate::address::{NodeAddress, SlotId};
use crate::live::dag::LiveDag;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::node::private::InterfaceNodePrivate;
use crate::node::{final_slot_data, Node, NodeProperties, NodeProperty, PropertyData};
use crate::pow_two::SizePow2;
use crate::prelude::*;
use serde::{Deserialize, Serialize};
use std::string::ToString;
use std::sync::Mutex;
use threadpool::ThreadPool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeFloatToGray {
    properties: NodeProperties,
}

const VALUE: &str = "value";

#[typetag::serde]
impl Node for NodeFloatToGray {
    fn new_unboxed() -> Self {
        NodeFloatToGray {
            properties: NodeProperties::new(vec![NodeProperty::new(
                VALUE,
                PropertyData::Float(0.0),
            )]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![Slot::new(SlotType::Gray, SlotId(0), "output")]
    }

    fn title(&self) -> String {
        "Float to Gray".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeFloatToGray {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        let live_node_id = live_dag.insert(creator_address, Box::new(self.clone()));

        live_dag.map_corresponding(creator_address, live_node_id, Side::Input, SlotId(0));
        live_dag.map_corresponding(creator_address, live_node_id, Side::Output, SlotId(0));
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        let value = final_slot_data(self.properties(), &process_data, VALUE);
        let SlotData::Float(value) = value else { panic!("expected a `SlotData::Float`") };
        let slot_image = SlotImage::gray_from_value(SizePow2::default(), value)
            .expect("could not create `SlotImage`");
        let slot_data = SlotData::SlotImage(slot_image);

        process_data.send(vec![(SlotId(0), slot_data)]).unwrap();
    }
}
