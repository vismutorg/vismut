use crate::address::{NodeAddress, SlotId};
use crate::live::dag::LiveDag;
use crate::live::node::ProcessData;
use crate::node::private::InterfaceNodePrivate;
use crate::node::{final_slot_data, Node, NodeProperties, NodeProperty, PropertyData};
use crate::prelude::*;
use serde::{Deserialize, Serialize};
use std::string::ToString;
use std::sync::Mutex;
use threadpool::ThreadPool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeFloat {
    properties: NodeProperties,
}

const VALUE: &str = "value";

#[typetag::serde]
impl Node for NodeFloat {
    fn new_unboxed() -> Self {
        NodeFloat {
            properties: NodeProperties::new(vec![NodeProperty::new(
                VALUE,
                PropertyData::Float(0.0),
            )]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![Slot::new(SlotType::Float, SlotId(0), "output")]
    }

    fn title(&self) -> String {
        "Float".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeFloat {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        let live_node_id = live_dag.insert(creator_address, Box::new(self.clone()));

        live_dag.map_corresponding(creator_address, live_node_id, Side::Input, SlotId(0));
        live_dag.map_corresponding(creator_address, live_node_id, Side::Output, SlotId(0));
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        let value = final_slot_data(self.properties(), &process_data, VALUE);

        process_data.send(vec![(SlotId(0), value)]).unwrap();
    }
}
