use crate::address::{Side, SlotId};
use crate::engine::VismutPixel;
use crate::error::{Result, VismutError};

use crate::live::node::ProcessData;
use crate::prelude::*;
use dyn_clone::DynClone;
use serde::{Deserialize, Serialize};
use std::any::Any;
use std::fmt::{Debug, Display, Formatter};
use std::sync::Arc;

mod prelude {
    pub(crate) use crate::{
        address::{NodeAddress, SlotId},
        live::{node::ProcessData, slot_data::SlotImage},
        node::{
            final_slot_data, pixel_buffer, private::InterfaceNodePrivate, Node, NodeProperties,
            NodeProperty, PropertyData,
        },
    };
    pub(crate) use serde::{Deserialize, Serialize};
    pub(crate) use std::sync::{Arc, Mutex};
    pub(crate) use threadpool::ThreadPool;
}

pub mod bool;
pub mod distribute_gray;
pub mod float;
pub mod float_to_gray;
pub mod gradient;
pub mod image;
pub mod int;
pub mod math_gray;
pub mod merge_rgba;
pub mod output_rgba;
mod output_rgba_private;
pub mod resize;
mod resize_private;
pub mod sample_uv;
pub mod split_rgba;

pub type VismutFloat = f32;
pub type VismutInt = i64;

/// Data stored in a node property.
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum PropertyData {
    /// A true/false value.
    Bool(bool),
    /// A decimal number.
    Float(VismutFloat),
    /// A grayscale pixel.
    Gray(VismutPixel),
    /// A whole number.
    Int(VismutInt),
    /// A mathematical operation such as add or multiply.
    MathOperation(MathOperation),
    /// Which algorithm to use when resizing an image.
    ResizeFilter(ResizeFilter),
    /// How to determine an image's size.
    ResizePolicy(ResizePolicy),
    /// A color + alpha pixel.
    Rgba([VismutPixel; 4]),
    /// A piece of text.
    String(String),
}

impl Display for PropertyData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                PropertyData::Bool(_) => "Bool",
                PropertyData::Float(_) => "Float",
                PropertyData::Gray(_) => "Gray",
                PropertyData::Int(_) => "Int",
                PropertyData::MathOperation(_) => "MathOperation",
                PropertyData::ResizeFilter(_) => "ResizeFilter",
                PropertyData::ResizePolicy(_) => "ResizePolicy",
                PropertyData::Rgba(_) => "Rgba",
                PropertyData::String(_) => "String",
            }
        )
    }
}

impl PropertyData {
    /// Any type in this list won't have a slot.
    fn has_slot(&self) -> bool {
        !matches!(
            self,
            Self::ResizePolicy(_)
                | Self::ResizeFilter(_)
                | Self::String(_)
                | Self::MathOperation(_)
        )
    }
}

/// A `PropertyData` with a name.
/// This is some data saved on a node.
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct NodeProperty {
    name: String,
    data: PropertyData,
}

impl NodeProperty {
    pub(crate) fn new(name: &str, data: PropertyData) -> Self {
        Self {
            name: name.to_string(),
            data,
        }
    }
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn data(&self) -> &PropertyData {
        &self.data
    }
    fn data_mut(&mut self) -> &mut PropertyData {
        &mut self.data
    }
}

mod private {
    use crate::live::dag::LiveDag;
    use crate::live::node::ProcessData;

    use crate::prelude::*;
    use std::sync::Mutex;
    use threadpool::ThreadPool;

    pub trait InterfaceNodePrivate {
        /// Inserts a set of LiveEdges and LiveNodes into the LiveDag based on this node.
        /// These are the nodes that actually get calculated.
        fn activate(&self, _live_dag: &mut LiveDag, _creator_address: NodeAddress) {
            panic!(
                "this node has not implemented activate,\
            probably because it only exists in the LiveGraph,\
            where nodes never get activated"
            )
        }

        /// Calculates the outputs of the node and sends it back to the `Engine`.
        fn process(&self, _thread_pool: &Mutex<ThreadPool>, _process_data: ProcessData) {
            panic!(
                "this node has not implemented process,\
            probably because it only exists as an interface node,\
            so it should never be processed"
            )
        }
    }
}

/// This is the trait that is used to interact with nodes from the outside.
#[typetag::serde(tag = "node")]
pub trait Node: private::InterfaceNodePrivate + DynClone + Debug + Sync + Send {
    /// Creates a new node,
    /// boxes it and returns it.
    ///
    /// This is a convenience function,
    /// since you will probably want a boxed node most of the time.
    ///
    /// This should not be manually implemented.
    fn new() -> Box<Self>
    where
        Self: Sized,
    {
        Box::new(Self::new_unboxed())
    }

    /// Creates a new node.
    fn new_unboxed() -> Self
    where
        Self: Sized;

    /// Returns a list of all input slots.
    /// This generally doesn't need to be manually implemented.
    fn slots_input(&self) -> Vec<Slot> {
        self.properties().slots()
    }

    /// Returns a list of all output slots.
    fn slots_output(&self) -> Vec<Slot>;

    /// Returns a list of all slots on the given side.
    /// This should not be manually implemented.
    fn slots(&self, side: Side) -> Vec<Slot> {
        match side {
            Side::Input => self.slots_input(),
            Side::Output => self.slots_output(),
        }
    }

    /// Returns a slot with the given name on the given side if it exists.
    ///
    /// Returns an error if there is no slot on that side with that name.
    ///
    /// This should not be manually implemented.
    fn slot_from_name(&self, side: Side, name: &str) -> Result<Slot> {
        self.slots(side)
            .iter()
            .find(|slot| slot.name == name)
            .map_or(
                Err(VismutError::InvalidSlotName(name.to_string())),
                |slot| Ok(slot.clone()),
            )
    }

    /// Returns a slot on the given `Side` with the given `SlotId`.
    ///
    /// Returns an error if that `SlotId` does not exist on that `Side`.
    ///
    /// This should not be manually implemented.
    fn slot_from_id(&self, side: Side, slot_id: SlotId) -> Result<Slot> {
        self.slots(side)
            .iter()
            .find(|&s| s.slot_id == slot_id)
            .map_or(Err(VismutError::InvalidSlotId(slot_id)), |slot| {
                Ok(slot.clone())
            })
    }

    /// Checks if there is a slot on the given `Side` with the given `SlotId`.
    ///
    /// Returns an error if that `SlotId` does not exist on that `Side`.
    /// Otherwise it returns `Ok`.
    ///
    /// This should not be manually implemented.
    fn slot_id_exists(&self, side: Side, slot_id: SlotId) -> Result<()> {
        self.slots(side)
            .iter()
            .find(|&s| s.slot_id == slot_id)
            .map_or(Err(VismutError::InvalidSlotId(slot_id)), |_| Ok(()))
    }

    /// Returns the text that should be shown for this node in a graphical interface.
    fn title(&self) -> String;

    /// Returns a reference to the `NodeProperties` of the node.
    /// This is how you check the properties on the node.
    fn properties(&self) -> &NodeProperties;

    /// Returns a reference to the property on the node with the given name.
    fn property(&self, name: &str) -> Result<&PropertyData> {
        self.properties().get(name)
    }

    /// Returns a mutable reference to the `NodeProperties` of the node.
    /// This is how you change the properties on the node.
    fn properties_mut(&mut self) -> &mut NodeProperties;

    /// Returns a mutable reference to the property on the node with the given name.
    fn property_mut(&mut self, name: &str) -> Result<&mut PropertyData> {
        self.properties_mut().get_mut(name)
    }

    /// Make this return true for any type that should have its output exported.
    fn is_output(&self) -> bool {
        false
    }
}

impl PartialEq for dyn Node {
    fn eq(&self, other: &Self) -> bool {
        self.type_id() == other.type_id() && self.properties() == other.properties()
    }
}

dyn_clone::clone_trait_object!(Node);

/// The properties of a node.
/// Every node has one of these.
///
/// The purpose of this struct is to provide a reusable interface to getting and editing properties.
#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
pub struct NodeProperties {
    /// A vector of the properties on the node.
    /// Changing these properties changes the output of the node.
    ///
    /// It's private because adding or removing from this vector would probably create a mismatch
    /// between what the node expects and what is actually there.
    /// This means you need to create a new node if you want to change it.
    properties: Vec<NodeProperty>,
}

impl NodeProperties {
    /// Returns a new `NodeProperties` with the given properties.
    pub(crate) fn new(properties: Vec<NodeProperty>) -> Self {
        Self { properties }
    }

    /// Get a reference to a property with the given name.
    fn get_property(&self, name: &str) -> Result<&NodeProperty> {
        self.properties
            .iter()
            .find(|node_property| node_property.name == *name)
            .ok_or(VismutError::InvalidPropertyName(name.to_string()))
    }

    /// Get a mutable reference to a property with the given name.
    fn get_property_mut(&mut self, name: &str) -> Result<&mut NodeProperty> {
        self.properties
            .iter_mut()
            .find(|node_property| node_property.name == *name)
            .ok_or(VismutError::InvalidPropertyName(name.to_string()))
    }

    /// Get a reference to the data of a property with the given name.
    pub fn get(&self, name: &str) -> Result<&PropertyData> {
        self.get_property(name)
            .map(|node_property| &node_property.data)
    }

    /// Get a mutable reference to the data of a property with the given name.
    pub fn get_mut(&mut self, name: &str) -> Result<&mut PropertyData> {
        self.get_property_mut(name).map(NodeProperty::data_mut)
    }

    /// Generates and returns a list of input slots.
    /// The list is automatically generated from the properties on the node.
    pub fn slots(&self) -> Vec<Slot> {
        self.properties()
            .iter()
            .filter(|node_property| node_property.data.has_slot())
            .enumerate()
            .map(|(i, prop)| {
                let slot_type = SlotType::from(prop.data());
                Slot::new(slot_type, SlotId(i), &prop.name)
            })
            .collect()
    }

    /// Returns a list of the node's properties.
    /// This is useful when you want to for instance display the available properties.
    pub fn properties(&self) -> &Vec<NodeProperty> {
        &self.properties
    }
}

/// Returns the input on the given slot if there is one,
/// otherwise it returns the setting on the property.
///
/// This is a convenience function for use when processing nodes.
pub(crate) fn final_slot_data(
    properties: &NodeProperties,
    process_data: &ProcessData,
    name: &str,
) -> SlotData {
    // Get any input which may or may not be present on the slot.
    let slots = properties.slots();
    let slot = slots
        .iter()
        .find(|slot| slot.name == name)
        .unwrap_or_else(|| panic!("there was no slot with the given name: {}", name));
    let slot_data = process_data.get_slot_datas(&[slot.slot_id])[0];

    // If there was an input on the slot...
    if let Some(slot_data) = slot_data {
        // Use that.
        slot_data.clone()
    } else {
        // Otherwise,
        // get the `property` corresponding to that slot.
        properties
            .get(name)
            .expect("there was no property with the given name")
            .clone()
            .into()
    }
}

/// Convenience function to get a buffer the size of a pixel.
pub(crate) fn pixel_buffer(value: VismutPixel) -> Buffer {
    Buffer::from_raw(1, 1, vec![value]).unwrap()
}

/// Takes in a position and wraps it in the 0-1 space if desired.
///
/// Returns None if `wrap` is `false,
/// and the position is outside of the 0-1 space.
fn wrap_position(position: f32, wrap_point: f32, wrap: bool) -> Option<f32> {
    if wrap {
        Some(position.rem_euclid(wrap_point))
    } else if position < 0.0 || position >= wrap_point {
        None
    } else {
        Some(position)
    }
}

// todo: Add sampling filters like linear and nearest
fn sample_image(image: &Arc<Buffer>, x: f32, y: f32, wrap_x: bool, wrap_y: bool) -> VismutPixel {
    let x = wrap_position(x, image.width() as f32, wrap_x);
    let y = wrap_position(y, image.height() as f32, wrap_y);

    if let (Some(x), Some(y)) = (x, y) {
        image.get_pixel(x as u32, y as u32)[0]
    } else {
        0.0
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum MathOperation {
    Add,
    Subtract,
    Multiply,
    Divide,
    Power,
    SquareRoot,
    Sin,
    Cos,
    Max,
    Min,
}

impl Default for MathOperation {
    fn default() -> Self {
        Self::Add
    }
}

impl Display for MathOperation {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Add => write!(f, "Add"),
            Self::Subtract => write!(f, "Subtract"),
            Self::Multiply => write!(f, "Multiply"),
            Self::Divide => write!(f, "Divide"),
            Self::Power => write!(f, "Power"),
            Self::SquareRoot => write!(f, "Square Root"),
            Self::Sin => write!(f, "Sin"),
            Self::Cos => write!(f, "Cos"),
            Self::Max => write!(f, "Max"),
            Self::Min => write!(f, "Min"),
        }
    }
}
