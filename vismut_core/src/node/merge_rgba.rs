use super::prelude::*;
use crate::address::{NodeAddress, SlotId};
use crate::live::dag::LiveDag;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::node::private::InterfaceNodePrivate;
use crate::node::resize_private::NodeResizePrivate;
use crate::node::{final_slot_data, Node, NodeProperties, NodeProperty, PropertyData};
use crate::prelude::*;

use serde::{Deserialize, Serialize};
use std::string::ToString;
use threadpool::ThreadPool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeMergeRgba {
    properties: NodeProperties,
}

const RED: &str = "red";
const GREEN: &str = "green";
const BLUE: &str = "blue";
const ALPHA: &str = "alpha";
const RESIZE_POLICY: &str = "resize_policy";
const RESIZE_FILTER: &str = "resize_filter";

#[typetag::serde]
impl Node for NodeMergeRgba {
    fn new_unboxed() -> Self {
        NodeMergeRgba {
            properties: NodeProperties::new(vec![
                NodeProperty::new(
                    RESIZE_POLICY,
                    PropertyData::ResizePolicy(ResizePolicy::default()),
                ),
                NodeProperty::new(
                    RESIZE_FILTER,
                    PropertyData::ResizeFilter(ResizeFilter::default()),
                ),
                NodeProperty::new(RED, PropertyData::Gray(0.0)),
                NodeProperty::new(GREEN, PropertyData::Gray(0.0)),
                NodeProperty::new(BLUE, PropertyData::Gray(0.0)),
                NodeProperty::new(ALPHA, PropertyData::Gray(1.0)),
            ]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![Slot::new(SlotType::Rgba, SlotId(0), "output")]
    }

    fn title(&self) -> String {
        "Merge RGBA".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeMergeRgba {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        let merge_rgba_node_id = live_dag.insert(creator_address, Box::new(self.clone()));

        let mut node_resize = NodeResizePrivate::new();

        // Configure the NodeResize.
        let resize_policy = self
            .properties()
            .get(RESIZE_POLICY)
            .expect("expected a `PropertyData::ResizePolicy`")
            .clone();
        *node_resize
            .properties_mut()
            .get_mut(RESIZE_POLICY)
            .expect("expected a `PropertyData::ResizePolicy`") = resize_policy;

        let resize_filter = self
            .properties()
            .get(RESIZE_FILTER)
            .expect("expected a `PropertyData::ResizeFilter`")
            .clone();
        *node_resize
            .properties_mut()
            .get_mut(RESIZE_FILTER)
            .expect("expected a `PropertyData::ResizeFilter`") = resize_filter;

        // Insert all properties in the NodeResize.
        node_resize.add_resize_slot(self, RED).unwrap();
        node_resize.add_resize_slot(self, GREEN).unwrap();
        node_resize.add_resize_slot(self, BLUE).unwrap();
        node_resize.add_resize_slot(self, ALPHA).unwrap();

        // Insert and hook up inputs for the NodeResize.
        node_resize
            .insert_resize_node(live_dag, creator_address, self, merge_rgba_node_id)
            .unwrap();

        // Map the output.
        live_dag.map_corresponding(creator_address, merge_rgba_node_id, Side::Output, SlotId(0));
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        // Get the buffer on each input slot.
        let SlotData::SlotImage(SlotImage::Gray(red)) = final_slot_data(self.properties(), &process_data, RED)
            else { panic!("expected a SlotImage::Gray") };
        let SlotData::SlotImage(SlotImage::Gray(green)) = final_slot_data(self.properties(), &process_data, GREEN)
            else { panic!("expected a SlotImage::Gray") };
        let SlotData::SlotImage(SlotImage::Gray(blue)) = final_slot_data(self.properties(), &process_data, BLUE)
            else { panic!("expected a SlotImage::Gray") };
        let SlotData::SlotImage(SlotImage::Gray(alpha)) = final_slot_data(self.properties(), &process_data, ALPHA)
            else { panic!("expected a SlotImage::Gray") };

        // Create the output RGBA slot image.
        let slot_image = SlotImage::Rgba([red, green, blue, alpha]);
        let slot_data = SlotData::SlotImage(slot_image);

        // Send it.
        process_data.send(vec![(SlotId(0), slot_data)]).unwrap();
    }
}
