use super::prelude::*;
use crate::address::{NodeAddress, SlotId};
use crate::live::dag::LiveDag;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::node::private::InterfaceNodePrivate;
use crate::node::{final_slot_data, Node, NodeProperties, NodeProperty, PropertyData};
use crate::prelude::*;
use image::Luma;
use serde::{Deserialize, Serialize};
use std::string::ToString;
use threadpool::ThreadPool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeGradient {
    properties: NodeProperties,
}

const VERTICAL: &str = "vertical";
const RESIZE_POLICY: &str = "resize_policy";

#[typetag::serde]
impl Node for NodeGradient {
    fn new_unboxed() -> Self {
        NodeGradient {
            properties: NodeProperties::new(vec![
                NodeProperty::new(
                    RESIZE_POLICY,
                    PropertyData::ResizePolicy(ResizePolicy::default()),
                ),
                NodeProperty::new(VERTICAL, PropertyData::Bool(false)),
            ]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![Slot::new(SlotType::Gray, SlotId(0), "output")]
    }

    fn title(&self) -> String {
        "Gradient".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeGradient {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        let live_node_id = live_dag.insert(creator_address, Box::new(self.clone()));

        live_dag.map_corresponding(creator_address, live_node_id, Side::Input, SlotId(0));
        live_dag.map_corresponding(creator_address, live_node_id, Side::Output, SlotId(0));
    }

    fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        let vertical = final_slot_data(self.properties(), &process_data, VERTICAL);
        let SlotData::Bool(vertical) = vertical else { panic!("expected a `SlotData::Bool`") };

        let resize_policy = self
            .properties()
            .get(RESIZE_POLICY)
            .unwrap_or_else(|_| panic!("expected property with name: {}", RESIZE_POLICY));
        let PropertyData::ResizePolicy(resize_policy) = resize_policy
            else { panic!("expected a `PropertyData::ResizePolicy`") };

        let size = resize_policy.size_no_input(process_data.dag_properties.size);

        let (width, height) = size.to_tuple();
        let width = width.result();
        let height = height.result();

        let thread_pool = thread_pool.lock().unwrap();
        thread_pool.execute(move || {
            let buffer = if vertical {
                Buffer::from_fn(width, height, |_x, y| Luma([y as f32 / height as f32]))
            } else {
                Buffer::from_fn(width, height, |x, _y| Luma([x as f32 / width as f32]))
            };

            let slot_data = SlotData::SlotImage(SlotImage::Gray(Arc::new(buffer)));

            process_data.send(vec![(SlotId(0), slot_data)]).unwrap();
        });
    }
}
