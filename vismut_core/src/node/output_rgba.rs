use super::prelude::*;

use crate::live::dag::LiveDag;
use crate::node::output_rgba_private::NodeOutputRgbaPrivate;

use crate::prelude::*;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeOutputRgba {
    properties: NodeProperties,
}

const NAME: &str = "name";
const INPUT: &str = "input";

#[typetag::serde]
impl Node for NodeOutputRgba {
    fn new_unboxed() -> Self {
        NodeOutputRgba {
            properties: NodeProperties::new(vec![
                NodeProperty::new(NAME, PropertyData::String("untitled".to_string())),
                NodeProperty::new(INPUT, PropertyData::Rgba([0.0, 0.0, 0.0, 1.0])),
            ]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        Vec::new()
    }

    fn title(&self) -> String {
        "Output RGBA".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }

    fn is_output(&self) -> bool {
        true
    }
}

impl InterfaceNodePrivate for NodeOutputRgba {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        // Clone the input property for the new node.
        let input_property = self
            .property(INPUT)
            .expect("expected the property to exist")
            .clone();

        // Create the new node and set its property.
        let mut output_rgba_private = NodeOutputRgbaPrivate::new();
        *output_rgba_private
            .property_mut(INPUT)
            .expect("expected the property to exist") = input_property;

        // Insert the new node.
        let new_node_id = live_dag.insert(creator_address, output_rgba_private);

        // Map the input to the new node.
        live_dag.map_corresponding_name(creator_address, new_node_id, Side::Input, self, INPUT);

        // We map the output slot even though it doesn't exist.
        // Because when it is requested it will find the correct output slot in the live world,
        // which lets us get the output of the node in the live world.
        live_dag.map_corresponding(creator_address, new_node_id, Side::Output, SlotId(0));
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, _process_data: ProcessData) {
        panic!("a `NodeOutputRgba` node should never be processed")
    }
}
