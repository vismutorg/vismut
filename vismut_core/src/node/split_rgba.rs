use super::prelude::*;
use crate::address::{NodeAddress, SlotId};
use crate::live::dag::LiveDag;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::node::private::InterfaceNodePrivate;
use crate::node::{final_slot_data, Node, NodeProperties, NodeProperty, PropertyData};
use crate::prelude::*;

use serde::{Deserialize, Serialize};
use std::string::ToString;
use threadpool::ThreadPool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeSplitRgba {
    properties: NodeProperties,
}

const RED: &str = "red";
const GREEN: &str = "green";
const BLUE: &str = "blue";
const ALPHA: &str = "alpha";
const INPUT: &str = "input";

#[typetag::serde]
impl Node for NodeSplitRgba {
    fn new_unboxed() -> Self {
        NodeSplitRgba {
            properties: NodeProperties::new(vec![NodeProperty::new(
                INPUT,
                PropertyData::Rgba([0.0, 0.0, 0.0, 1.0]),
            )]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![
            Slot::new(SlotType::Gray, SlotId(0), RED),
            Slot::new(SlotType::Gray, SlotId(1), GREEN),
            Slot::new(SlotType::Gray, SlotId(2), BLUE),
            Slot::new(SlotType::Gray, SlotId(3), ALPHA),
        ]
    }

    fn title(&self) -> String {
        "Split RGBA".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeSplitRgba {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        // Insert the node.
        let split_rgba_node_id = live_dag.insert(creator_address, Box::new(self.clone()));

        // Map the input.
        live_dag.map_corresponding_name(
            creator_address,
            split_rgba_node_id,
            Side::Input,
            self,
            INPUT,
        );

        // Map the outputs.
        live_dag.map_corresponding_name(
            creator_address,
            split_rgba_node_id,
            Side::Output,
            self,
            RED,
        );
        live_dag.map_corresponding_name(
            creator_address,
            split_rgba_node_id,
            Side::Output,
            self,
            GREEN,
        );
        live_dag.map_corresponding_name(
            creator_address,
            split_rgba_node_id,
            Side::Output,
            self,
            BLUE,
        );
        live_dag.map_corresponding_name(
            creator_address,
            split_rgba_node_id,
            Side::Output,
            self,
            ALPHA,
        );
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        // Get the input buffers.
        let SlotData::SlotImage(SlotImage::Rgba(buffers)) = final_slot_data(self.properties(), &process_data, INPUT)
            else { panic!("expected a SlotImage::Rgba") };

        // Create the output R, G, B, and A images.
        let red = SlotData::SlotImage(SlotImage::Gray(Arc::clone(&buffers[0])));
        let green = SlotData::SlotImage(SlotImage::Gray(Arc::clone(&buffers[1])));
        let blue = SlotData::SlotImage(SlotImage::Gray(Arc::clone(&buffers[2])));
        let alpha = SlotData::SlotImage(SlotImage::Gray(Arc::clone(&buffers[3])));

        // Send it.
        process_data
            .send(vec![
                (SlotId(0), red),
                (SlotId(1), green),
                (SlotId(2), blue),
                (SlotId(3), alpha),
            ])
            .unwrap();
    }
}
