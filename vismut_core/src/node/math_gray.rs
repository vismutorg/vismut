use crate::address::{NodeAddress, SlotId};
use crate::live::dag::LiveDag;

use crate::live::node::ProcessData;
use crate::node::prelude::*;
use crate::node::private::InterfaceNodePrivate;
use crate::node::resize_private::NodeResizePrivate;
use crate::node::{final_slot_data, Node, NodeProperties, NodeProperty, PropertyData};
use crate::prelude::*;
use image::Luma;
use serde::{Deserialize, Serialize};

use std::string::ToString;
use std::sync::Mutex;
use threadpool::ThreadPool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeMathGray {
    properties: NodeProperties,
}

const OPERATION: &str = "operation";
const LEFT_SIDE: &str = "left_side";
const RIGHT_SIDE: &str = "right_side";
const OUTPUT: &str = "output";

/// Performs mathematical operations on two input images.
#[typetag::serde]
impl Node for NodeMathGray {
    fn new_unboxed() -> Self {
        NodeMathGray {
            properties: NodeProperties::new(vec![
                NodeProperty::new(
                    OPERATION,
                    PropertyData::MathOperation(MathOperation::default()),
                ),
                NodeProperty::new(LEFT_SIDE, PropertyData::Gray(0.0)),
                NodeProperty::new(RIGHT_SIDE, PropertyData::Gray(0.0)),
            ]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![Slot::new(SlotType::Gray, SlotId(0), OUTPUT)]
    }

    fn title(&self) -> String {
        "Math Gray".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeMathGray {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        let math_gray_node_id = live_dag.insert(creator_address, Box::new(self.clone()));

        let mut node_resize = NodeResizePrivate::new();

        node_resize.add_resize_slot(self, LEFT_SIDE).unwrap();
        node_resize.add_resize_slot(self, RIGHT_SIDE).unwrap();

        node_resize
            .insert_resize_node(live_dag, creator_address, self, math_gray_node_id)
            .unwrap();

        live_dag.map_corresponding_name(
            creator_address,
            math_gray_node_id,
            Side::Output,
            self,
            OUTPUT,
        );
    }

    fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        let node_properties = self.properties().clone();

        let thread_pool = thread_pool.lock().unwrap();
        thread_pool.execute(move || {
            // Get the data we need.
            let SlotData::SlotImage(SlotImage::Gray(left_side)) = final_slot_data(&node_properties, &process_data, LEFT_SIDE)
                else { panic!("expected a SlotImage::Gray") };
            let SlotData::SlotImage(SlotImage::Gray(right_side)) = final_slot_data(&node_properties, &process_data, RIGHT_SIDE)
                else { panic!("expected a SlotImage::Gray") };
            let Ok(PropertyData::MathOperation(math_operation)) = node_properties.get(OPERATION)
                else { panic!("expected a PropertyData::MathOperation") };

            // Create the output.
            let buffer = Buffer::from_fn(left_side.width(), left_side.height(), |x, y| {
                match math_operation {
                    MathOperation::Add => Luma([left_side.get_pixel(x, y).0[0] + right_side.get_pixel(x, y).0[0]]),
                    MathOperation::Subtract => Luma([left_side.get_pixel(x, y).0[0] - right_side.get_pixel(x, y).0[0]]),
                    MathOperation::Multiply => Luma([left_side.get_pixel(x, y).0[0] * right_side.get_pixel(x, y).0[0]]),
                    MathOperation::Divide => Luma([left_side.get_pixel(x, y).0[0] / right_side.get_pixel(x, y).0[0]]),
                    MathOperation::Power => Luma([left_side.get_pixel(x, y).0[0].powf(right_side.get_pixel(x, y).0[0])]),
                    MathOperation::SquareRoot => Luma([left_side.get_pixel(x, y).0[0].sqrt()]),
                    MathOperation::Sin => Luma([left_side.get_pixel(x, y).0[0].sin()]),
                    MathOperation::Cos => Luma([left_side.get_pixel(x, y).0[0].cos()]),
                    MathOperation::Max => Luma([left_side.get_pixel(x, y).0[0].max(right_side.get_pixel(x, y).0[0])]),
                    MathOperation::Min => Luma([left_side.get_pixel(x, y).0[0].min(right_side.get_pixel(x, y).0[0])]),
                }
            });

            // Wrap the output in the appropriate types.
            let slot_data = SlotData::SlotImage(SlotImage::Gray(Arc::new(buffer)));

            // Send it.
            process_data.send(vec![(SlotId(0), slot_data)]).unwrap();
        });
    }
}
