use crate::address::{NodeAddress, SlotId};
use crate::live::dag::LiveDag;

use crate::live::node::ProcessData;
use crate::node::prelude::*;
use crate::node::private::InterfaceNodePrivate;
use crate::node::{
    final_slot_data, sample_image, Node, NodeProperties, NodeProperty, PropertyData,
};
use crate::prelude::*;
use image::Luma;
use serde::{Deserialize, Serialize};

use glam::{Affine2, Vec2};
use rand::{Rng, SeedableRng};
use std::string::ToString;
use std::sync::Mutex;
use threadpool::ThreadPool;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NodeDistributeGray {
    properties: NodeProperties,
}

const RESIZE_POLICY: &str = "resize_policy";
const IMAGE: &str = "image";
const COUNT_X: &str = "count_x";
const COUNT_Y: &str = "count_y";
const SCALE_X: &str = "scale_x";
const SCALE_Y: &str = "scale_y";
const RANDOM_X: &str = "random_x";
const RANDOM_Y: &str = "random_y";
const OUTPUT: &str = "output";

/// Duplicates and places an input image more or less randomly to create a noise or pattern.
#[typetag::serde]
impl Node for NodeDistributeGray {
    fn new_unboxed() -> Self {
        NodeDistributeGray {
            properties: NodeProperties::new(vec![
                NodeProperty::new(
                    RESIZE_POLICY,
                    PropertyData::ResizePolicy(ResizePolicy::default()),
                ),
                NodeProperty::new(IMAGE, PropertyData::Gray(0.0)),
                NodeProperty::new(COUNT_X, PropertyData::Int(1)),
                NodeProperty::new(COUNT_Y, PropertyData::Int(1)),
                NodeProperty::new(SCALE_X, PropertyData::Float(1.0)),
                NodeProperty::new(SCALE_Y, PropertyData::Float(1.0)),
                NodeProperty::new(RANDOM_X, PropertyData::Float(0.0)),
                NodeProperty::new(RANDOM_Y, PropertyData::Float(0.0)),
            ]),
        }
    }

    fn slots_output(&self) -> Vec<Slot> {
        vec![Slot::new(SlotType::Gray, SlotId(0), OUTPUT)]
    }

    fn title(&self) -> String {
        "Distribute Gray".to_string()
    }

    fn properties(&self) -> &NodeProperties {
        &self.properties
    }

    fn properties_mut(&mut self) -> &mut NodeProperties {
        &mut self.properties
    }
}

impl InterfaceNodePrivate for NodeDistributeGray {
    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress) {
        // Insert the node.
        let distribute_gray_node_id = live_dag.insert(creator_address, Box::new(self.clone()));

        // Map the inputs.
        live_dag.map_corresponding_name(
            creator_address,
            distribute_gray_node_id,
            Side::Input,
            self,
            IMAGE,
        );
        live_dag.map_corresponding_name(
            creator_address,
            distribute_gray_node_id,
            Side::Input,
            self,
            COUNT_X,
        );
        live_dag.map_corresponding_name(
            creator_address,
            distribute_gray_node_id,
            Side::Input,
            self,
            COUNT_Y,
        );
        live_dag.map_corresponding_name(
            creator_address,
            distribute_gray_node_id,
            Side::Input,
            self,
            SCALE_X,
        );
        live_dag.map_corresponding_name(
            creator_address,
            distribute_gray_node_id,
            Side::Input,
            self,
            SCALE_Y,
        );
        live_dag.map_corresponding_name(
            creator_address,
            distribute_gray_node_id,
            Side::Input,
            self,
            RANDOM_X,
        );
        live_dag.map_corresponding_name(
            creator_address,
            distribute_gray_node_id,
            Side::Input,
            self,
            RANDOM_Y,
        );

        // Map the output.
        live_dag.map_corresponding_name(
            creator_address,
            distribute_gray_node_id,
            Side::Output,
            self,
            OUTPUT,
        );
    }

    fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        let node_properties = self.properties().clone();

        let thread_pool = thread_pool.lock().unwrap();
        thread_pool.execute(move || {
            // Prepare all our properties for use.
            let SlotData::SlotImage(image) =
                final_slot_data(&node_properties, &process_data, IMAGE)
                else { panic!("expected input image to exist") };
            let SlotData::Int(count_x) =
                final_slot_data(&node_properties, &process_data, COUNT_X)
                else { panic!("expected input count_x to exist") };
            let SlotData::Int(count_y) =
                final_slot_data(&node_properties, &process_data, COUNT_Y)
                else { panic!("expected input count_y to exist") };
            let SlotData::Float(scale_x) =
                final_slot_data(&node_properties, &process_data, SCALE_X)
                else { panic!("expected input scale_x to exist") };
            let SlotData::Float(scale_y) =
                final_slot_data(&node_properties, &process_data, SCALE_Y)
                else { panic!("expected input scale_y to exist") };
            let SlotData::Float(random_x) =
                final_slot_data(&node_properties, &process_data, RANDOM_X)
                else { panic!("expected input random_x to exist") };
            let SlotData::Float(random_y) =
                final_slot_data(&node_properties, &process_data, RANDOM_Y)
                else { panic!("expected input random_y to exist") };

            let Ok(PropertyData::ResizePolicy(resize_policy)) = node_properties.get(RESIZE_POLICY)
                else { panic!("expected a resize_policy to exist") };
            let output_size_pow =
                resize_policy.size(image.size(), process_data.dag_properties.size);
            let (output_width, output_height) = output_size_pow.result();

            let (input_image_width, input_image_height) = image.size().result();

            let scale = Vec2::new(scale_x, scale_y);

            let ratio = Vec2::new(
                output_width as f32 / input_image_width as f32,
                output_height as f32 / input_image_height as f32,
            );

            let scale_ratio = (ratio / Vec2::new(count_x as f32, count_y as f32)) * scale;

            let mut rng = rand_xorshift::XorShiftRng::seed_from_u64(0);

            let matrices = {
                let mut matrices = Vec::with_capacity((count_x * count_y * 9) as usize);

                for y in 0..count_y {
                    for x in 0..count_x {
                        let offset = Vec2::new(
                            rng.gen::<f32>() * random_x - (random_x / 2.0),
                            rng.gen::<f32>() * random_y - (random_y / 2.0),
                        ) * scale;

                        let pattern_size = Vec2::new(
                            output_width as f32 / count_x as f32,
                            output_height as f32 / count_y as f32,
                        );

                        let translation =
                            Vec2::new(x as f32 * pattern_size.x, y as f32 * pattern_size.y)
                                + offset;

                        let center_translation = Vec2::new(0.5, 0.5);

                        // The transformation of each sprite.
                        let transformation = Affine2::from_translation(center_translation)
                            * Affine2::from_scale_angle_translation(scale_ratio, 0.0, translation)
                                .inverse();

                        let width = output_width as f32;
                        let height = output_height as f32;

                        // Create each matrix nine times,
                        // to make it look like it wraps around the image at the edges and corners.
                        //
                        // Note that matrices that won't make a difference should be culled.
                        // That culling is not implemented at the time of writing.

                        // Top row
                        matrices.push(
                            transformation * Affine2::from_translation(Vec2::new(-width, -height)),
                        );
                        matrices.push(
                            transformation * Affine2::from_translation(Vec2::new(0.0, -height)),
                        );
                        matrices.push(
                            transformation * Affine2::from_translation(Vec2::new(width, -height)),
                        );

                        // Middle row
                        matrices.push(
                            transformation * Affine2::from_translation(Vec2::new(-width, 0.0)),
                        );
                        matrices
                            .push(transformation * Affine2::from_translation(Vec2::new(0.0, 0.0)));
                        matrices.push(
                            transformation * Affine2::from_translation(Vec2::new(width, 0.0)),
                        );

                        // Bottom row
                        matrices.push(
                            transformation * Affine2::from_translation(Vec2::new(-width, height)),
                        );
                        matrices.push(
                            transformation * Affine2::from_translation(Vec2::new(0.0, height)),
                        );
                        matrices.push(
                            transformation * Affine2::from_translation(Vec2::new(width, height)),
                        );
                    }
                }

                matrices
            };

            let buffer = Buffer::from_fn(output_width, output_height, |x, y| {
                let point = Vec2::new(x as f32, y as f32);

                let result = matrices
                    .iter()
                    .map(|matrix| matrix.transform_point2(point))
                    .map(|point| sample_image(image.inner_gray(), point.x, point.y, false, false))
                    .fold(0.0, |acc, x| acc + x);

                Luma([result])
            });

            let slot_data = SlotData::SlotImage(SlotImage::Gray(Arc::new(buffer)));

            process_data.send(vec![(SlotId(0), slot_data)]).unwrap();
        });
    }
}
